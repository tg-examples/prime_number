<?php

function isPrime($n){
    if($n < 2){
        return false; //gdy liczba jest mniejsza niż 2 to nie jest pierwszą
    }
    if($n > 2 && $n%2 == 0){
        return false; // gdy liczba jest parzysta i większa od 2
    }

    for($i=2; $i*$i<=$n; $i++) {
        if($n%$i == 0){
            return false; //gdy znajdziemy dzielnik, to dana liczba nie jest pierwsza
        }
    }
    return true;
}

$number = $argv[1];

if(isPrime($number)){
    echo "Liczba {$number} jest pierwsza";
} else {
    echo "Liczba {$number} nie jest pierwsza";
}